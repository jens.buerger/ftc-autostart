# FTC Autostart

Autostart for the Fischertechnik TXT Community Firmware

## Deploy

To deploy the autostart script just follow the following steps:

1. Install [pssh](https://linux.die.net/man/1/pssh)
2. Specify the host you want to deploy to in the `deploy/hosts` file
3. Execute `do-everything.sh`

:warning: By default `do-everything.sh` will also disable the TXT's firewall. 
To prevent this comment out `sh 01-remove-firewall.sh` in the script. 

## Adapt the Autostart Script

To adapt the autostart behavior...

* Before the deployment change `src/autostart.sh`
* After the deployment change `/home/ftc/autostart.sh`