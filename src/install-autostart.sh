#! /bin/sh

AUTOSTART_SCRIPT=/home/ftc/autostart.sh
AUTOSTART_INITD_SCRIPT=/etc/init.d/S99autostart-script.sh

# user autostart script

cp autostart.sh ${AUTOSTART_SCRIPT}
chmod +x ${AUTOSTART_SCRIPT}


# install init.d script that starts autostart script

sudo cp S99autostart-script.sh ${AUTOSTART_INITD_SCRIPT}
sudo chmod +x ${AUTOSTART_INITD_SCRIPT}
sudo chown root ${AUTOSTART_INITD_SCRIPT}
sudo chgrp root ${AUTOSTART_INITD_SCRIPT}
