#! /bin/sh

# This script automatically launches a bash script during startup

NAME=autostart-script
AUTOSTART_SCRIPT=/home/ftc/autostart.sh


case "$1" in
  start)
    printf "Starting $NAME: "
    sh ${AUTOSTART_SCRIPT} &
    [ $? = 0 ] && echo "OK" || echo "FAIL"
    ;;
  stop)
    printf "Stopping $NAME: "
    pkill -f "${AUTOSTART_SCRIPT}"
    [ $? = 0 ] && echo "OK" || echo "FAIL"
    ;;
  restart|reload)
    echo "Restarting $NAME: "
    $0 stop
    sleep 1
    $0 start
    ;;
  *)
    echo "Usage: $0 {start|stop|restart|reload}" >&2
    exit 1
    ;;
esac

exit 0
